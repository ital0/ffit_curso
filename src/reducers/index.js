import { combineReducers } from 'redux';

import ffitReducer from './ffitReducer';

const appReducer = combineReducers({
    ffit: ffitReducer,
});

export default appReducer;