import {
    COR_DO_TEXTO,
    UPDATE_CONTADOR,
    UPDATE_CONTADOR1,
    UPDATE_CONTADOR2,
} from "../constants";

const INITIAL_STATE = {
    textColor: 'black',
    counter: 0,
    finalObject: {
        counter1: 0,
        counter2: 0,
    }
};

export default function ffitReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        default:
            return state;
        case COR_DO_TEXTO:
            return {
                ...state,
                textColor: 'brown',
            };
        case UPDATE_CONTADOR1:
            return {
                ...state,
                finalObject: {
                    counter1: state.finalObject.counter1 + action.payload.incrementValue,
                    counter2: state.finalObject.counter2,
                }
            };
        case UPDATE_CONTADOR2:
            return {
                ...state,
                finalObject: {
                    counter1: state.finalObject.counter1,
                    counter2: state.finalObject.counter2 + action.payload.incrementValue,
                }
            };
    }
}