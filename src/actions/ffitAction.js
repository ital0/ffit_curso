import {
    COR_DO_TEXTO,
    UPDATE_CONTADOR,
    UPDATE_CONTADOR1,
    UPDATE_CONTADOR2,
} from "../constants";

export const updateCorDoTexto = (newColor) => (dispatch) => {
  dispatch({
      type: COR_DO_TEXTO,
      action: {
          payload: {
              newTextColor: newColor,
          }
      }
  });
};

export const incrementCounter = (type, incrementValue) => (dispatch) => {
  dispatch({
      type: (type === 1) ? UPDATE_CONTADOR1 : UPDATE_CONTADOR2,
      payload: {
          incrementValue,
      }
  });
};

