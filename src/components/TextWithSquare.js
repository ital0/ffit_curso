import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as ffitActions from '../actions/ffitAction';

class TextWithSquare extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            ffit,
            ffitActions,
        } = this.props;
        return (
            <TouchableOpacity onPress={() => {
                ffitActions.updateCorDoTexto('brown');
            }}>
                <Text style={{color: ffit.textColor, fontSize: 32, }}>
                    TEXTO
                </Text>
            </TouchableOpacity>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ffit: state.ffit,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        ffitActions: bindActionCreators(ffitActions, dispatch),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(TextWithSquare);