import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as ffitActions from "../actions/ffitAction";

class ButtonWithCounter extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            ffit,
            ffitActions,
            dispatch,
        } = this.props;

        return (
            <View style={{
                flex: 1,
                backgroundColor: 'gray',
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <View style={{flex: 0.5}}>
                    <View style={{ flex: 0.3}}>
                        <Text style={{fontSize: 32,}}>Contador 1: {ffit.finalObject.counter1}</Text>
                    </View>
                    <View style={{ flex: 0.7}}>
                        <TouchableOpacity onPress={() => {
                            ffitActions.incrementCounter(1, 1);
                        }}>
                            <Text style={{fontSize: 32,}}>Incrementar Counter 1</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{flex: 0.5}}>
                    <View style={{ flex: 0.3}}>
                        <Text style={{fontSize: 32,}}>Contador 2: {ffit.finalObject.counter2}</Text>
                    </View>
                    <View style={{ flex: 0.7}}>
                        <TouchableOpacity onPress={() => {
                            ffitActions.incrementCounter(2, 1);
                        }}>
                            <Text style={{fontSize: 32,}}>Incrementar Counter 2</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ffit: state.ffit,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        ffitActions: bindActionCreators(ffitActions, dispatch),
        dispatch: dispatch,
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ButtonWithCounter);