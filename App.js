import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Provider, connect } from 'react-redux';
import configureStore from './src/config/configStore';
import TextWithSquare from "./src/components/TextWithSquare";
import ButtonWithCounter from "./src/components/ButtonWithCounter";

export default class App extends Component<{}> {
  constructor(props) {
    super(props);

    this.store = configureStore();
  }
  
  render() {
    return (
        <Provider store={this.store}>
          <View style={{flex: 1}}>
            <ButtonWithCounter />
          </View>
        </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
